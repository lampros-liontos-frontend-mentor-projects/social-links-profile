# Frontend Mentor - Social links profile solution

This is a solution to the [Social links profile challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/social-links-profile-UG32l9m6dQ). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Overview](#overview)
- [The challenge](#the-challenge)
- [Screenshot](#screenshot)
- [Links](#links)
- [My process](#my-process)
- [Built with](#built-with)
- [What I learned](#what-i-learned)
- [Continued development](#continued-development)
- [Useful resources](#useful-resources)
- [Author](#author)

**Note: Delete this note and update the table of contents based on what sections you keep.**

## Overview

In this project, I created a basic social link-sharing profile.  It has been personalized to use my own information, but I attempted to keep it as close to the original as possible.  I also added a slight transition to the hover coloring, to make the website feel a little more dynamic.

### The challenge

Users should be able to:

- See hover and focus states for all interactive elements on the page

### Screenshot

![](./screenshot-375.png)
![](./screenshot-1440.png)

### Links

- Solution URL: [GitLab Project Page](https://gitlab.com/lampros-liontos-frontend-mentor-projects/social-links-profile)
- Live Site URL: [Live Netlify App Site](https://ll-fm-social-links-profile.netlify.app)

## My process

I first edited the HTML file to do the following:
1. Added the "Inter" font from Google APIs.
2. Added a link to the `index.css` stylesheet.
3. For each of the avatar picture, name, location, and quote, I grouped into separate `section` environments with their own classnames.
4. I did the same with the navbar, but I used the `nav` environment.
5. I surrounded the `sections` with an overarching `main` environment.
6. I moved the attribution into its own `footer` section, underneath `main`.
7. I added the `attribution` class to both the `footer` section, as well as the various `a` link tags in that section, to keep them organized.

Once the HTML was in place, I created a `index.css` file for the stylesheet.

I first created a `:root` environment to cover global variables.
1. I assigned colors and a section padding size to global variables.
2. I then set up a universal environment (`*`) to set up some defaults applying to all elements.
3. I set up the "Inter" font to be the default font family, and also added `sans-serif` to function as a backup in case the "Inter" font failed.
4. Used the `font-size` as specified in the style guide.  Anything other than paragraphs would be manually altered as needed.
5. Set the `font-weight` to 400, as specified in the style guide, for the default text weight.  As with `font-size` anything else will be used when needed.
6. Aside from these, I otherwise copied the font style code from the Google font page in order to ensure consistency overall.
7. I also added a `border-radius` setting, since all boxes were going to have rounded corners.

I then set up the `body` and `main` environments, first setting up the colors, using off-black and dark gray as their respective backgrounds.

Then, focusing on the `body`, I did the following:
1. Assigned the `margin` and `padding` to `0px`, thereby ensuring that the next step wouldn't result in scrollbars for the whole page.
2. I then assigned `height` and `width` to `100vh` and `100vw`respectively, in order to ensure that the page container is exactly the size of the browser window.  This ensures that the content would be perfectly centered bydefault.
3. I finally set up a page-wide `flex` container, to keep everything centered, and make sure all elements were added in a vertical direction.

Once the `body` was handled the `main` section was next, carrying the defaults for all its contents:
1. The `padding` made sure that no content was up against the `main` section border.
2. The `width` would be set to a value close as I could get to the design images.  I could probably measure based on pixels, but I'm used to using `em` units at this point.
3. The `text-align` was set to `center`, ensuring that all the text, including the buttons, were centered in the box.
4. A `flex` container was set up, ensuring that all the elements were aligned vertically.
5. I also placed a smaller amount of padding on the top and bottom, to ensure that the space of the top and bottom of the box matches the design screenshot.

I then proceeded to work on the individual `section` environments.  For each of them I added a `padding` element, set to the `--section-padding` value defined in `:root`.

I performed some other changes as well:
* For the `avatar` image, I changed the `border-radius`  a lot wider, to ensure that the picture, which was originally a square image, was rendered as a circle instead, as one would expect with a social profile avatar.
* For the `name` section, I changed the `font-weight` to 700, making it as bold as I could.  I also increased the `font-size` to make it stand out, as a good heading should.
* The `location` section had its text color changed to the `green` color, and the `font-weight` was set to 600, making it look bold, but not as bold as the name.
* The `nav` bar would be next, setting up yet another `flex` container.  Its `width` would be 95% of the `main` environment, allowing the navbar buttons to stay a little distance from the navbar border (and yet again match the design screenshot).
* The `attribution` footer section was transferred directly from the original HTML to the stylesheet, and no real changes were made.

The `social` links required a bit of work to get right, but I eventually came up with the following to handle them.
1. The background color was set to the `--gray` variable value.
2. The `font-weight` was set to 600 to ensure that the links were bold.
3. Each button had a `margin` of `0.6em` to provide space between the buttons.
4. They were also provided a `padding` of `1em` to provide space between the text and the top and bottom edges of the buttons.
5. As for the left and right sides, they were stretched out by making the `width` to `100%` of the `nav` section.  Since the `main` has text set to `center`, the text would automatically be centered in the buttons.
6. Buttons don't generally have underlines, so `text-decoration` was set to `none`.
7. I wanted to make the hover/focus change a bit animated, so I set the `transition-duration` to `0.15s`.  This means that, when the button is either focused (using the keyboard), or hovered over (using the mouse), the button would gradually shift from its normal coloring to its active coloring over the course of 3/20th of a second.  This keeps the page feeling responsive while ensuring it also has a dynamic feel.

I also set up a combined `:hover` and `:focus` section for `a.social` to swap the colors for the buttons; instead of white on dark gray, it becomes dark gray on green.

I had performed all the above working off of the mobile design image first.  However, since the sizing in the mobile and desktop versions were different, I also added a `@media` section which would expand the box when the page reaches a minimum of 600 pixels.

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- Mobile-first workflow
- [Inter Font](https://fonts.google.com/specimen/Inter)

### What I learned

I mostly refined what I learned from the last two projects (QR Code Component and Blog Preview Card), and was more careful about setting up the semantic HTML so that every separate component was in its own section, that they all were in a `<main>` section, and that they had matching classes to make selection easier (for example, the image was set for the class `avatar`, same as its containing `<section>`).  I also put more effort into placing shared values in higher containers in the CSS; for example, things that match over all sections would be placed in the `main` section, and anything shared between the `main` and `footer` would be set in `*`.  I am getting more comfortable using the variables to maintain consistent values, and allow an easy way to make changes that will be applied to everything.

### Continued development

Use this section to outline areas that you want to continue focusing on in future projects. These could be concepts you're still not completely comfortable with or techniques you found useful that you want to refine and perfect.

**Note: Delete this note and the content within this section and replace with your own plans for continued development.**

### Useful resources

- [DevDocs](https://devdocs.io/) - As usual, I use the documentation from this website to look up the various CSS commands I need to refresh myself on.
- [Google Fonts](https://fonts.google.com) - Source for the "Inter" font.  Also provided the boilerplate to include in the HTML and CSS to integrate the font.

## Author

- Website - [Lampros Liontos](#)
- Frontend Mentor - [@reteov](https://www.frontendmentor.io/profile/reteov)
- X (Twitter) - [@reteov](https://www.x.com/reteov)
 
